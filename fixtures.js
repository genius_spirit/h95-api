const mongoose = require('mongoose');
const config = require("./config");
const Product = require('./models/Task');
const User = require('./models/User');

mongoose.connect(config.db.url + "/" + config.db.name);

const db = mongoose.connection;

const collections = ['categories', 'products', 'users'];

db.once("open", async () => {

  collections.forEach(async collectionName => {
    try {
      await db.dropCollection(collectionName);
    } catch (e) {
      console.log(`Collection ${collectionName} did not exist in DB`);
    }
  });

  const [cpuCategory, hddCategory] = await Category.create({
    title: 'CPUs',
    description: 'Central Processing Units'
  }, {
    title: 'HDDs',
    description: 'Hard Disk Drives'
  });

   await Product.create({
    title: 'Intel Core i7',
    price: 300,
    description: "Very fast processor",
    category: cpuCategory._id,
    image: 'cpu.jpg'
  }, {
    title: 'Seagate 3TB',
    price: 100,
    description: "Big disc",
    category: hddCategory._id,
    image: 'hdd.jpg'
  });

   await User.create({
     username: 'user',
     password: '123',
     role: 'user'
   }, {
     username: 'admin',
     password: 'admin123',
     role: 'admin'
   });

  db.close();
});