const express = require("express");
const Task = require("../models/Task");
const auth = require('../middleware/auth');

const createRouter = () => {
const router = express.Router();

  router.get("/", auth, async(req, res) => {
    try {
      const task = await Task.find().sort({startDatetime: -1});
      if (task) res.send(task);
    } catch (e) {
      return res.status(500).send({error: e})
    }
  });

  router.post("/", auth, async(req, res) => {
    const data = {
      user: req.user._id,
      title: req.body.title,
      startDatetime: req.body.startDatetime,
      endDatetime: req.body.endDatetime,
    };

    const task = new Task(data);
    try {
      const response = await task.save();
      if (response) res.send(response);
    } catch (e) {
      return res.status(400).send({error: e});
    }
  });

  return router;
};

module.exports = createRouter;
